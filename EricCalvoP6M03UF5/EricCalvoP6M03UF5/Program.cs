﻿using System;
using System.IO;

namespace EricCalvoP6M03UF5
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine(divideixoCero(7, 2));
            Console.WriteLine(divideixoCero(8, 4));
            Console.WriteLine(divideixoCero(5, 0));

            Console.WriteLine(aDoubleoU("7,1"));
            Console.WriteLine(aDoubleoU("9,"));
            Console.WriteLine(aDoubleoU(",2"));
            Console.WriteLine(aDoubleoU("tres"));

            Fitxers("fitxerInexistent.txt");

            MutableList<string> list = new MutableList<string>();
            list.Add("a");
            list.Add("b");
            list.Add("c");
            list.Add("d");
            list.Add("e");
            list.Add("f");
            list.Borrador(2);
            list.Borrador(5);
        }

        static int divideixoCero(int divisor, int dividend)
        {
            try
            {
                return divisor / dividend;
            }
            catch (ArithmeticException)
            {
                return 0;
            }
        }

        static double aDoubleoU(string a)
        {
            try
            {
                return Convert.ToDouble(a);
            }
            catch (Exception)
            {
                return 1.0;
            }
        }

        static void Fitxers(string path)
        {
            try
            {
                File.ReadAllLines(path);
            }
            catch (IOException a)
            {
                Console.WriteLine($"S'ha produit un error d'entrada/sortida: \n{a}");
            }
        }
    }
}
