﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EricCalvoP6M03UF5
{
    class MutableList<T>
    {
        T[] list { get; set; }
        int i { get; set; }

        public MutableList()
        {
            this.list = new T[5];
            this.i = 0;
        }

        public MutableList(int z)
        {
            this.list = new T[z];
            this.i = 0;
        }

        public void Add(T value)
        {
            try
            {
                this.list[this.i] = value;
                this.i++;
                Console.WriteLine($"S'ha afegit '{value}'");
            }
            catch (IndexOutOfRangeException error)
            {
                Console.WriteLine("ERROR:" + error);
            }
        }

        public void Borrador(int index)
        {
            try
            {
                Console.WriteLine($"S'ha esborrat '{this.list[index]}'");
                this.list[index] = default(T);
            }
            catch (IndexOutOfRangeException error)
            {
                Console.WriteLine("ERROR:" + error);
            }

        }

        public void Visualize()
        {
            foreach (T val in this.list)
            {
                Console.WriteLine(val);
            }
        }

    }
}
